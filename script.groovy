
def buildImage() {
    echo "building the docker image..."
    withCredentials([usernamePassword(credentialsId: 'docker-hub', passwordVariable: 'PASS', usernameVariable: 'USER')]) {
        sh "docker build -t datoxx/my-repo:$IMAGE_NAME ."
        sh "echo $PASS | docker login -u $USER --password-stdin"
        sh "docker push datoxx/my-repo:$IMAGE_NAME"
    }
} 

def deployApp() {
    echo 'deploying the application...'
    def dockerCmd = 'docker run -p 3080:3080 -d datoxx/my-repo:js-example-1.0'
    sshagent(['ec2-server-key']) {
        sh "ssh -o StrictHostKeyChecking=no ec2-user@16.171.24.119 ${dockerCmd}"
    }
} 



return this
